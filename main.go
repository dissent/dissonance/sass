package main

import (
	"context"
	"log"
	"os"

	"github.com/bwmarrin/discordgo"
)

var Session, _ = discordgo.New()

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("gg.cry.srv.sass"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()

	eventPub := micro.NewPublisher("gg.cry.srv.sass", service.Client())

	// Discord Authentication Token
	Session.Token = os.Getenv("DG_TOKEN")
	if Session.Token == "" {
		log.Fatal("No Discord authentication token provided\n")
	}

	if err := Session.Open(); err != nil {
		log.Fatal(err)
	}

	Session.AddHandler(func(s *discordgo.Session, e *discordgo.Event) {
		if err := eventPub.Publish(context.Background(), e); err != nil {
			log.Logf("Failed to publish Discord event: %v\n", e.Type)
		}
	})

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
