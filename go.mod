module sass

go 1.12

require github.com/bwmarrin/discordgo v0.19.0

require github.com/micro/go-micro v1.1.0

require (
	github.com/armon/circbuf v0.0.0-20190214190532-5111143e8da2 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/micro/go-log v0.1.0
)
