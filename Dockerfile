# This file is a template, and might need editing before it works on your project.
FROM golang:alpine AS builder

RUN apk --no-cache add git ca-certificates

# set workdir to current service in GOPATH
WORKDIR /app/sass

# copy code into workdir
COPY . .
RUN go mod download

# build the binary, with alpine relevant flags
RUN CGO_ENABLE=0 GOOS=linux go build -v -o sass

FROM alpine:latest
# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app

COPY --from=builder /app/sass/sass .
CMD ["./sass"]
