# Sasscat Service

[![pipeline status](https://gitlab.com/dissent/dissonance/sass/badges/master/pipeline.svg)](https://gitlab.com/dissent/dissonance/sass/commits/master)
[![coverage report](https://gitlab.com/dissent/dissonance/sass/badges/master/coverage.svg)](https://gitlab.com/dissent/dissonance/sass/commits/master)

This project is a microservice implementation of the "gateway" component of the Discord API; a WebSocket API used primarily to receive realtime events from Discord, such as new messages.

It implements an aspect of an architecture similar to that described in [DasWolke's whitepaper](https://gist.github.com/DasWolke/c9d7dfe6a78445011162a12abd32091d) on chatbots using a microservice architecture.

The documentation for this project will be expanded further, however for now it may be helpful to refer the [the whitepaper](https://gist.github.com/DasWolke/c9d7dfe6a78445011162a12abd32091d) linked above.

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: gg.cry.srv.sasscat
- Type: srv
- Alias: sasscat

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./sasscat-srv
```

Build a docker image
```
make docker
```